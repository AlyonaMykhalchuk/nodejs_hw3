const mongoose = require('mongoose');

const userSchema = new mongoose.Schema( {
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
  },
  created_date: {
    type: String,
    default: new Date(),
  },
});
const User = mongoose.model('user', userSchema);

module.exports = User;
