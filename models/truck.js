const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    default: 'OS',
    required: true,
  },
  created_date: {
    type: Date,
    default: new Date(),
  },
  type: {
    type: String,
    required: true,
  },
});
const Truck = mongoose.model('truck', truckSchema);

module.exports = Truck;
