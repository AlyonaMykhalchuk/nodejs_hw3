const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
  created_by: {
    required: true,
    type: String,
  },
  logs: {
    type: Array,
    default:
            [{
              message: {
                type: String,
              },
              time: {
                type: Date,
                default:
                        Date.now(),
              },
            }],
  },
  assigned_to: {
    type: String,
  },
  created_date: {
    type: Date,
    default: new Date(),
  },
  status: {
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
    default: '',
  },
  name: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    default:
            {
              width: {
                type: Number,
                required: true,
              },
              length: {
                type: Number,
                required: true,
              },
              height: {
                type: Number,
                required: true,
              },
            },
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
});
const Load = mongoose.model('load', loadSchema);

module.exports = Load;
