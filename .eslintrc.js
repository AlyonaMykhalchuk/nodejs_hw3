module.exports = {
  'extends': ['eslint:recommended', 'google'],
  'env': {
    'node': true,
    'browser': true,
    'es6': true,
  },
  'parserOptions': {
    'ecmaVersion': 2019,
    'sourceType': 'module',
    'ecmaFeatures': {
      arrowFunctions: true,
      defaultParams: true,
    },
  },
  'rules': {
    'no-console': 'off',
    'max-len': ['error', {'code': 150, 'ignoreComments': true}],
    'quotes': ['warn', 'single', {'avoidEscape': true, 'allowTemplateLiterals': true}],
    'arrow-spacing': ['error', {'before': true, 'after': true}],
    'camelcase': 'off',
    'new-cap': 'off',
  },
};
