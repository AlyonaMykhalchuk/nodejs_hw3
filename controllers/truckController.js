const Truck = require('../models/truck');
const User = require('../models/user');

module.exports.createTruck = async (request, response) => {
  const typeOfTruck = request.body.type.toUpperCase();
  const userId = request.user._id;
  const driver = await User.findById(userId).exec();
  if (driver.role === 'DRIVER') {
    const truck = new Truck({created_by: userId, type: typeOfTruck});
    await truck.save()
        .then(() => {
          response.status(200).json({message: 'Truck created successfully'});
        })
        .catch((err) => {
          response.status(500).json({message: err.message});
        });
  }
  return response.status(400).json({message: 'Only drivers can add trucks'});
};

module.exports.assignTruck = async (request, response) => {
  const truckId = request.params.id;
  const {_id, role} = request.user;
  if (role !== 'DRIVER') {
    return response.status(400).json({message: 'Only drivers can assign truck'});
  }
  try {
    const hasAssignedTruck = await Truck.findOne({assigned_to: _id}).exec();
    if (hasAssignedTruck) {
      hasAssignedTruck.assigned_to = null;
      hasAssignedTruck.status ='OS';
      await hasAssignedTruck.save();
    }
    await Truck.findOne({_id: truckId, status: 'OS'}).exec()
        .then(async (truck) => {
          if (!truck) {
            return response.status(404).json({message: 'Invalid truck\'s id'});
          }
          if (truck.created_by === _id) {
            truck.assigned_to = _id;
            truck.status = 'IS';
            await truck.save();
            response.status(200).json({message: 'Truck assigned successfully'});
          } else {
            return response.status(400).json({message: 'You don\'t have access to this truck'});
          }
        });
  } catch (err) {
    response.status(500).json({message: err.message});
  }
};
module.exports.getAllTrucks = (request, response) => {
  const created_by = request.user._id;
  Truck.find({created_by}).exec()
      .then((data) => {
        if (!data) {
          return response.status(400).json({message: 'No trucks found for this user'});
        }
        response.status(200).json({trucks: data});
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.getTruckById = (request, response) => {
  const _id = request.params.id;
  const userId = request.user._id;
  Truck.findOne({_id}).exec()
      .then((truck) => {
        if (!truck) {
          return response.status(404).json({message: 'Invalid truck\'s id'});
        }
        if (truck.created_by === userId) {
          response.status(200).json({truck: truck});
        } else {
          return response.status(400).json({message: 'You don\'t have access to this truck'});
        }
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.updateNotAssignedTruck = async (request, response) => {
  const _id = request.params.id;
  const typeOfTruck = request.body.type.toUpperCase();
  const userId = request.user._id;
  Truck.findOne({_id}).exec()
      .then((truck) => {
        if (!truck) {
          return response.status(404).json({message: 'Invalid truck\'s id'});
        }
        if (truck.created_by === userId && !truck.assigned_to) {
          Truck.findByIdAndUpdate(_id, {type: typeOfTruck}).exec()
              .then(() => {
                response.status(200).json({message: 'Truck details changed successfully'});
              })
              .catch((err) => {
                response.status(500).json({message: err.message});
              });
        } else {
          return response.status(400).json({message: 'You don\'t have access to this truck'});
        }
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.deleteNotAssignedTruck = async (request, response) => {
  const _id = request.params.id;
  const userId = request.user._id;
  Truck.findOne({_id}).exec()
      .then((truck) => {
        if (!truck) {
          return response.status(404).json({message: 'Invalid truck\'s id'});
        }
        if (truck.created_by === userId && !truck.assigned_to) {
          Truck.findByIdAndDelete(_id).exec()
              .then(() => {
                response.status(200).json({message: 'Truck deleted successfully'});
              })
              .catch((err) => {
                response.status(500).json({message: err.message});
              });
        } else {
          return response.status(400).json({message: 'You don\'t have access to this truck'});
        }
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

