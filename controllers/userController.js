const User = require('../models/user');
const bcrypt = require('bcrypt');
const {SALT} = require('../config/config');

module.exports.getUser = (request, response) => {
  const {_id} = request.user;
  User.findById({_id}).exec()
      .then((user) => {
        if (!user) {
          return response.status(400).json({message: 'User not found'});
        }
        response.status(200).json({
          user: {_id: user._id, email: user.email, created_date: user.created_date},
        });
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};

module.exports.deleteUser = (request, response) => {
  const {_id} = request.user;

  User.findByIdAndDelete({_id}).exec()
      .then((user) => {
        if (!user) {
          return response.status(400).json({message: 'User not found'});
        }
        response.status(200).json({message: 'Profile deleted successfully'});
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};
module.exports.changePassword = async (request, response) => {
  try {
    const {oldPassword, newPassword} = request.body;
    const {_id} = request.user;

    if (!oldPassword) {
      return response.status(400).json({message: 'No old password found'});
    }
    if (!newPassword) {
      return response.status(400).json({
        message: 'No new password  found',
      });
    }
    const hashedNewPass = await bcrypt.hash(newPassword, SALT);

    User.findByIdAndUpdate({_id}, {password: hashedNewPass}).exec()
        .then(async (user) => {
          if (!user) {
            return response.status(400).json({message: 'No such register user'});
          }
          const rightPassword = await bcrypt.compare(oldPassword, user.password);
          if (rightPassword) {
            return response.json({message: 'Password changed successfully'});
          }
          return response.status(400).json({message: 'Invalid Password'});
        })
        .catch((err) => {
          response.status(500).json({message: err.message});
        });
  } catch (e) {
    response.status(500).json({message: e.message});
  }
};


