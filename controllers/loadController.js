const Load = require('../models/load');
const Truck = require('../models/truck');
const {truckTypesDb} = require('../config/truckTypeDb');

module.exports.createLoad = async (request, response) => {
  try {
    const {name, payload, dimensions, pickup_address, delivery_address} = request.body;
    const userId = request.user._id;
    const role = request.user.role;
    if (role !== 'SHIPPER') {
      return response.status(400).json({message: 'Only shippers can add loads'});
    }
    const load = new Load({
      created_by: userId,
      name: name,
      payload: payload,
      pickup_address: pickup_address,
      delivery_address: delivery_address,
      dimensions: {
        width: dimensions.width,
        length: dimensions.length,
        height: dimensions.height,
      },
      logs: [{
        message: '"Load created successfully"',
        time: new Date(),
      }],

    });
    await load.save()
        .then(() => {
          response.status(200).json({message: 'Load created successfully'});
        })
        .catch((err) => {
          response.status(500).json({message: err.message});
        });
  } catch (err) {
    response.status(500).json({message: err.message});
  }
};
module.exports.getAllLoads = async (request, response) => {
  const {status} = request.query;
  let loads;
  try {
    if (!status) {
      loads = await Load.find({$or: [{created_by: request.user._id}, {assigned_to: request.user._id}]});
    } else {
      loads = await Load.find({
        status: status.toUpperCase(),
        $or: [{created_by: request.user._id}, {assigned_to: request.user._id}],
      });
    }
    response.status(200).json({loads});
  } catch (err) {
    response.status(500).json({message: err.message});
  }
};

module.exports.getLoad = (request, response) => {
  const _id = request.params.id;
  const userId = request.user._id;
  Load.findById(_id).exec()
      .then((load) => {
        if (!load) {
          return response.status(404).json({message: 'Invalid load\'s id'});
        }
        if (load.created_by === userId || load.assigned_to === userId) {
          response.status(200).json({load: load});
        } else {
          return response.status(400).json({message: 'You don\'t have access to this load'});
        }
      })
      .catch((err) => {
        response.status(500).json({message: err.message});
      });
};
module.exports.nextLoadState = async (request, response) => {
  const userId = request.user._id;
  const userRole = request.user.role;
  if (userRole === 'DRIVER') {
    try {
      const load = await Load.findOne({status: 'ASSIGNED', assigned_to: userId});
      if (!load) {
        return response.status(400).json({message: 'There is no active load for you'});
      }
      const newLogs = load.logs;
      let message;
      let state;
      switch (load.state) {
        case '':
          state = 'En route to Pick Up';
          message = 'Load En route to Pick Up';
          break;
        case 'En route to Pick Up':
          state = 'Arrived to Pick Up';
          message = 'Load Arrived to Pick Up';
          break;
        case 'Arrived to Pick Up':
          state = 'En route to delivery';
          message = 'Load En route to delivery';
          break;
        case 'En route to delivery':
          state = 'Arrived to delivery';
          message = 'Load arrived to delivery';
          newLogs.push({
            message: message,
            time: new Date(),
          });
          await Load.findByIdAndUpdate(load._id, {state: state, status: 'SHIPPED', logs: newLogs}).exec();
          await Truck.findOneAndUpdate({status: 'OL', assigned_to: request.user._id}, {status: 'IS'});
          return response.status(200).json({message: `Load was successfully shipped`});
        default:
          return;
      }
      if (state) {
        load.logs.push({
          message: `Load state changed to ${state}`,
          time: new Date(),
        });
        await Load.findByIdAndUpdate(load._id, {state: state, logs: load.logs})
            .then(() => {
              response.status(200).json({message: `Load state changed to ${state}`});
            })
            .catch((err) => {
              response.status(500).json({message: err.message});
            });
      }
    } catch (err) {
      response.status(500).json({message: err.message});
    }
  }
  return response.status(400).json({message: 'You don\'t have access to this load'});
};

module.exports.getActiveLoads = async (request, response) => {
  const assign_to = request.user._id;
  const userRole = request.user.role;
  if (userRole !== 'DRIVER') {
    return response.status(400).json({message: 'You don\'t have access to these loads'});
  }
  try {
    const load = await Load.findOne({status: 'ASSIGNED', assigned_to: assign_to});
    if (!load) {
      return response(400).json({message: 'There is no active load'});
    }
    response.status(200).json({load});
  } catch (err) {
    response.status(500).json({message: err.message});
  }
};

module.exports.updateNewLoad = async (request, response) => {
  const loadId = request.params.id;
  const {name, payload, pickup_address, delivery_address, width, length, height} = request.body;
  const userId = request.user._id;
  let newLogs;
  await Load.findById(loadId).exec()
      .then((load) => {
        newLogs = load.logs;
        newLogs.push({
          message: 'Load details changed successfully',
          time: new Date(),
        });
        Load.findByIdAndUpdate(loadId, {
          ...load,
          created_by: userId, name: name,
          payload: payload,
          pickup_address: pickup_address,
          delivery_address: delivery_address,
          dimensions: {
            width: width,
            length: length,
            height: height,
          },
          logs: newLogs,
        }).exec()
            .then(() => {
              response.status(200).json({message: 'Load details changed successfully'});
            })
            .catch((err) => {
              response.status(500).json({message: err.message});
            });
      }).catch(() => {
        response.status(400).json({message: 'You coudn`t update this load'});
      });
};

module.exports.deleteNewLoad = async (request, response) => {
  try {
    const load = await Load.findOne({_id: request.params.id, created_by: request.user._id, status: 'NEW'});
    if (!load) {
      return response.status(400).json({message: 'There is no load with such id or the load is active'});
    }
    await Load.deleteOne({_id: request.params.id, created_by: request.user._id});
    response.status(200).json({message: 'Load deleted successfully'});
  } catch (err) {
    response.status(500).json({message: err.message});
  }
};

module.exports.postLoad = async (request, response, next) => {
  const loadId = request.params.id;

  try {
    const load = await Load.findById(loadId);
    const newLogs = load.logs;
    if (load.status !== 'NEW') {
      response.status(400).json({message: 'You can post only new load'});
    }
    load.status = 'POSTED';
    await load.save();
    const truckTypes = truckTypesDb.filter((item) => item.payload >= load.payload &&
            item.dimensions.length >= load.dimensions.length &&
            item.dimensions.width >= load.dimensions.width &&
            item.dimensions.height >= load.dimensions.height,
    ).map((item) => item.type);
    const truck = await Truck.findOne({status: 'IS', type: {$in: truckTypes}}).exec();
    if (!truck) {
      newLogs.push({
        message: 'Load posted successfully.', driver_found: false,
        time: new Date(),
      });
      load.status = 'NEW';
      load.logs = newLogs;
      await load.save();
      return response.status(200).json({message: 'There are no trucks for your load'});
    }
    truck.status = 'OL';
    const driverId = truck.assigned_to;
    newLogs.push({
      message: 'Load posted successfully', driver_found: true,
      time: new Date(),
    });
    load.status = 'ASSIGNED';
    load.assigned_to = driverId;
    load.state = 'En route to Pick Up';
    load.logs = newLogs;
    await load.save();
    await truck.save();
    response.status(200).json({message: 'Load posted successfully', driver_found: true});
  } catch (err) {
    response.status(500).json({message: err.message});
  }
};

module.exports.viewShippingInfo = async (request, response) => {
  const loadId = request.params.id;
  await Load.findById(loadId).exec()
      .then(async (load) => {
        await Truck.findOne({assigned_to: load.assigned_to}).exec()
            .then((truck) => {
              response.status(200).json({load: load, truck: truck});
            }).catch((err) => {
              response.status(500).json({message: err.message});
            });
      }).catch((err) => {
        response.status(500).json({message: err.message});
      });
};
