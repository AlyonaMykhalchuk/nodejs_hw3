const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/user');
const {SECRET, SALT} = require('../config/config');
const generator = require('generate-password');
const nodemailer = require('nodemailer');
const nodemailerMailgun = require('nodemailer-mailgun-transport');
const {API_KEY, DOMAIN}=require('../config/config');
const auth = {
  auth: {
    api_key: API_KEY,
    domain: DOMAIN,
  },
};


module.exports.registerUser = async (request, response) => {
  try {
    const {email, password} = request.body;
    const role = request.body.role.toUpperCase();
    const passwordHash = await bcrypt.hash(password, SALT);
    const user = new User({email: email, password: passwordHash, role: role});
    await user.save()
        .then(() => {
          response.status(200).json({message: 'Profile created successfully'});
        })
        .catch((err) => {
          if (err.isJoi === true) {
            err.status(400).json({message: err.isJoi.message});
          }
          response.status(500).json({message: err.message});
        });
  } catch (err) {
    response.status(500).json({message: err.message});
  }
};

module.exports.loginUser = async (request, response) => {
  try {
    const {email, password} = request.body;
    const user = await User.findOne({email: email}).exec();
    if (!user) {
      return response.status(400).json({message: 'No user with such username and password'});
    }
    const rightPassword = await bcrypt.compare(password, user.password);
    if (rightPassword) {
      return response.status(200).json({jwt_token: jwt.sign(JSON.stringify(user), SECRET)});
    }
    return response.status(500).json({message: 'wrong password'});
  } catch (err) {
    response.status(500).json({message: err.message});
  }
};

module.exports.forgotPassword = async (request, response) => {
  try {
    const {email} = request.body;
    const user = await User.findOne({email: email}).exec();
    if (!user) {
      return response.status(400).json({message: 'No user with such email'});
    }
    const newPass = generator.generate({
      length: 10,
      numbers: true,
    });
    user.password = await bcrypt.hash(newPass, SALT);
    await user.save();
    const transporter = nodemailer.createTransport(nodemailerMailgun(auth));
    const mailOptions = {
      from: 'Excited User <me@samples.mailgun.org>',
      to: `${email}`,
      subject: 'Forgot password?',
      text: `Your new password is ${newPass}`,
    };
    await transporter.sendMail((mailOptions));
    response.status(200).json({message: 'New password sent to your email', newPass});
  } catch (err) {
    response.status(500).json({message: err.message});
  }
};

