const express = require('express');
const router = express.Router();
const {joiLoginSchema, joiRegisterSchema, joiForgotPassSchema} = require('../validation/joiSchema');
const validator = require('express-joi-validation').createValidator({
  passError: true,
});

const {registerUser, loginUser, forgotPassword} = require('../controllers/authController');

router.post('/register', validator.body(joiRegisterSchema), registerUser);
router.post('/login', validator.body(joiLoginSchema), loginUser);
router.post('/forgot_password', validator.body(joiForgotPassSchema), forgotPassword);

module.exports = router;
