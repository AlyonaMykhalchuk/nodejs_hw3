const express = require('express');
const router = express.Router();
const {joiChangePassSchema} = require('../validation/joiSchema');
const validator = require('express-joi-validation').createValidator({
  passError: true,
});
const {getUser, deleteUser, changePassword} = require('../controllers/userController');

router.get('/', getUser);
router.delete('/', deleteUser);
router.patch('/password', validator.body(joiChangePassSchema), changePassword);

router.use((err, req, res, next) => {
  if (err && err.error && err.error.isJoi) {
    res.status(400).json({message: err.error.toString()});
  } else {
    next(err);
  }
});

module.exports = router;
