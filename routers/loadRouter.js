const express = require('express');
const router = express.Router();
const {joiLoadSchema} = require('../validation/joiSchema');
const validator = require('express-joi-validation').createValidator({
  passError: true,
});

const {createLoad, getLoad, getAllLoads,
  nextLoadState, getActiveLoads, updateNewLoad,
  deleteNewLoad, postLoad, viewShippingInfo} = require('../controllers/loadController');

router.get('/', getAllLoads);
router.post('/', validator.body(joiLoadSchema), createLoad);
router.get('/active', getActiveLoads);
router.patch('/active/state', nextLoadState);
router.get('/:id', getLoad);
router.put('/:id', updateNewLoad);
router.delete('/:id', deleteNewLoad);
router.post('/:id/post', postLoad);
router.get('/:id/shipping_info', viewShippingInfo);

module.exports = router;
