const express = require('express');
const router = express.Router();
const {createTruck, assignTruck, getTruckById, getAllTrucks,
  deleteNotAssignedTruck, updateNotAssignedTruck} = require('../controllers/truckController');
const {joiTruckSchema} = require('../validation/joiSchema');
const validator = require('express-joi-validation').createValidator({
  passError: true,
});

router.post('/', validator.body(joiTruckSchema), createTruck);
router.post('/:id/assign', assignTruck);
router.get('/:id', getTruckById);
router.get('/', getAllTrucks);
router.put('/:id', validator.body(joiTruckSchema), updateNotAssignedTruck);
router.delete('/:id', deleteNotAssignedTruck);

module.exports = router;
