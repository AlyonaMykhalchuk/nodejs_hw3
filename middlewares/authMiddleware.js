const jwt = require('jsonwebtoken');
const {SECRET} = require('../config/config');

module.exports = (request, response, next) => {
  const authHeader = request.headers['authorization'];
  if (!authHeader) {
    return response.status(401).json({status: 'No authorization found'});
  }
  const [, jwtToken] = authHeader.split(' ');
  try {
    request.user = jwt.verify(jwtToken, SECRET);
    next();
  } catch (err) {
    return response.status(401).json({status: 'Invalid JWT'});
  }
};
