const joi = require('joi');

const joiRegisterSchema = joi.object({
  email: joi.string().email().lowercase().required(),
  password: joi.string().required(),
  role: joi.string().valid(...['DRIVER', 'SHIPPER']).required().insensitive(),
});

const joiLoginSchema = joi.object({
  email: joi.string().email().lowercase().required(),
  password: joi.string().required(),
});
const joiForgotPassSchema = joi.object({
  email: joi.string().email().lowercase().required(),
});

const joiLoadSchema = joi.object({
  name: joi.string().required(),
  payload: joi.number().required(),
  pickup_address: joi.string().required(),
  delivery_address: joi.string().required(),
  dimensions: joi.object({
    width: joi.number().required(),
    length: joi.number().required(),
    height: joi.number().required(),
  }).required(),
  state: joi.string().validate(...['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']),
  status: joi.string().validate(...['NEW', 'POSTED', 'ASSIGNED', ' SHIPPED']),
  logs: joi.array(),
  created_date: joi.date(),
  created_by: joi.string(),
  assigned_to: joi.string(),
});

const joiTruckSchema = joi.object({
  type: joi.string().valid(...['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']).required().insensitive(),
});
const joiChangePassSchema = joi.object({
  oldPassword: joi.string().required(),
  newPassword: joi.string().required(),
});
module.exports = {joiRegisterSchema, joiLoginSchema, joiLoadSchema, joiTruckSchema, joiForgotPassSchema, joiChangePassSchema};
