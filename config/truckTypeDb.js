const truckTypesDb = [
  {
    'type': 'SPRINTER',
    'dimensions': {
      'width': 300,
      'height': 250,
      'length': 170,
    },
    'payload': 1700,
  },
  {
    'type': 'SMALL STRAIGHT',
    'dimensions': {
      'width': 500,
      'height': 250,
      'length': 170,
    },
    'payload': 2500,
  },
  {
    'type': 'LARGE STRAIGHT',
    'dimensions': {
      'width': 700,
      'height': 350,
      'length': 200,
    },
    'payload': 4000,
  },
];

module.exports = {
  truckTypesDb,
};
