const config = require('config');
const {dbUser, dbPassword, dbName, dbHost} = config.get('Customer.dbConfig');
const {host, port} = config.get('Customer.server');
const {jwt_secret, salt} = config.get('Customer.auth');
const {api_key, domain} = config.get('Customer.mailgun');

module.exports = {
  DB_USER: dbUser,
  DB_PASSWORD: dbPassword,
  DB_NAME: dbName,
  DB_HOST: dbHost,
  SECRET: jwt_secret,
  SALT: salt,
  PORT: port,
  HOST: host,
  API_KEY: api_key,
  DOMAIN: domain,
};
