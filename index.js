const express = require('express');
const mongoose = require('mongoose');
const {PORT, DB_PASSWORD, DB_USER, DB_NAME} = require('./config/config');
const loadRouter = require('./routers/loadRouter');
const truckRouter = require('./routers/truckRouter');
const userRouter = require('./routers/userRouter');
const authsRouter = require('./routers/authsRouter');
const authMiddleware = require('./middlewares/authMiddleware');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
const fs = require('fs');
const path = require('path');

mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}@cluster0.zzmpz.mongodb.net/${DB_NAME}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
}).then(() => console.log('DB connected'))
    .catch((err) => console.log(err.message));

app.use(express.json());
app.use(cors());

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});
app.use(morgan('common', {
  stream: accessLogStream,
}));

app.use('/api/auth', authsRouter);
app.use(authMiddleware);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

app.use((err, req, res, next) => {
  if (err && err.error && err.error.isJoi) {
    res.status(400).json({message: err.error.toString()});
  } else {
    next(err);
  }
});

app.listen(PORT, () => {
  console.log(`Server listens on ${PORT} port`);
});
